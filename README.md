Hawky-one is a web frontend and API that permits you to create a set of images, train them and creates a pattern.

It works in conjuction with the Botorosa Service: https://gitlab.com/the-vision/botorosa

## Architecture

Below there is a scratch on how works this crazy idea:

![Architecture](https://gitlab.com/the-vision/hawky-one/raw/master/doc/img/architecture.png)

## Steps

1. Setup a new *knowledge*
2. Upload the images to a dataset
3. Train the set
4. Create a image pattern
5. Use the REST API to apply the pattern to any image